//! Additional Definitions
//!
//! # Admin Assistant
//!
//! (Actor)
//!
//! An employee who works on-site receiving [Donation](#donation) and forwarding
//! relevant info to [Bookkeeper](#bookkeeper)
//!
//! # Bookkeeper
//!
//! (Actor)
//!
//! A contractor who works off-site entering [Donation](#donation) into
//! [CRM Software](#crm-software)
//!
//! # Check
//!
//! (Data)
//!
//! A check is an instruction to transfer money from account owned by
//! [Donor](#donor) to account owned by [Client](#client). Each check
//! is identifiable via [Check Metadata](#check-metadata)
//!
//! # Client
//!
//! (Actor)
//!
//! A non-profit organization who takes [Donation](#donation)
//!
//! # Constituent File
//!
//! (Data)
//!
//! A record of all [Donor](#donor) contact data and all past [Donation](#donation)
//! stored in [CRM Software](#crm-software)
//!
//! # CRM Software
//!
//! (External Interface)
//!
//! A database containing [Constituent File](#constituent-file)
//!
//! For the scope of this project, [CRM Software](#crm-software):
//!
//! -   Can have a custom field added to each
//!     [Constituent File](#constituent-file) for one or more
//!     [`CheckHash`][crate::data_object::CheckHash]
//! -   Can search via said custom field for single
//!     [`CheckHash`][crate::data_object::CheckHash]
//!
//! # Donation
//!
//! (Event)
//!
//! [Donor](#donor) gives [Client](#client) money. In the scope of this project it
//! is assumed that the medium is a [Check](#check)
//!
//! # Donor
//!
//! (Actor)
//!
//! A person or organization donating money to [Client](#client)
//!
//! # Financial Institution
//!
//! (External Interface)
//!
//! Bank or similar organization with checking account held by [Client](#client) or
//! [Donor](#donor)
//!
//! # Unintended 3rd Party
//!
//! (Actor)
//!
//! A hacker, or any other party who should not have access to
//! [Check Metadata](#check-metadata)
