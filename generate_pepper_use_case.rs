//! Generate a `Pepper` for use in [`crate::calculate_check_hash_use_case`]
//!
//! # Problem
//!
//! ## Subjective
//!
//! ### When `User`
//!
//! 1. Wants to try project
//! 2. Loads form
//!
//! ### What `User` expects
//!
//! - Some way to generate a 'Pepper'
//!
//! ### What actually happens
//!
//! - Form with an empty `Pepper` field
//!
//! ## Objective
//!
//! - See definition of `Salt` and actors
//!
//! ## Assessment
//!
//! - Selecting a random `Pepper` should be done programmatically

use crate::data_object::Pepper;

pub mod prelude {
    pub use super::*;
}

pub trait RandomHex {
    fn rand_hex(&mut self) -> u8;
}
pub enum Error {}
pub struct RequestModel<R: RandomHex> {
    pub randomizer: R,
}

#[derive(PartialEq, Eq)]
pub struct ResponseModel {
    pub pepper: Pepper,
}

use crate::data_object::array_of_base::Error as ArrayOfBaseError;

trait TryFromVecExact {
    fn try_from_vec_exact(
        &self,
    ) -> std::result::Result<Pepper, ArrayOfBaseError>;
}

impl TryFromVecExact for [u8] {
    fn try_from_vec_exact(
        &self,
    ) -> std::result::Result<Pepper, ArrayOfBaseError> {
        Pepper::try_from_vec_exact(self)
    }
}

impl From<Pepper> for Result {
    fn from(pepper: Pepper) -> Self {
        Ok(ResponseModel { pepper })
    }
}

pub type Result = std::result::Result<ResponseModel, Error>;

/// Generate a random 256 bit [`Pepper`]
///
/// # Errors
///
/// This use case has no error states
///
/// # Example
///
/// ```
/// # use check_hash_calculator::generate_pepper_use_case::prelude::*;
/// # use check_hash_calculator::data_object::Pepper;
/// #
/// # #[derive(Default)]
/// # struct DummyRandomizer {
/// #     base: u8,
/// # }
/// #
/// # impl RandomHex for DummyRandomizer {
/// #     fn rand_hex(&mut self) -> u8 {
/// #         let result = self.base;
/// #         self.base += 1;
/// #         assert!(self.base < 17);
/// #         result
/// #     }
/// # }
/// #
/// // GIVEN
/// let randomizer = DummyRandomizer::default();
/// // As it is impossible to test the result of a random number generator
/// // a sequential number generator is used for the test.
///
/// // WHEN
/// let result_model = generate_pepper(RequestModel { randomizer });
///
/// // THEN
/// assert!(
///     result_model.ok().unwrap()
///         == ResponseModel {
///             pepper: Pepper::try_from_vec_exact(&[
///                 0u8, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
///             ])
///             .unwrap(),
///         },
/// );
/// ```
pub fn generate_pepper<R: RandomHex>(
    mut request_model: RequestModel<R>,
) -> Result {
    [0u8; 16]
        .map(|_| request_model.randomizer.rand_hex())
        .try_from_vec_exact()
        .expect("1667680867 - Unreachable")
        .into()
}
