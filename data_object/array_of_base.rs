//! An array of specified length `N` of u8 values from 0 to `B`-1

/// An array of specified length `N` of u8 values from 0 to `B`-1
///
/// # Example - Success
/// ```
/// # use check_hash_calculator::data_object::ArrayOfBase;
/// // GIVEN
/// let array = [0, 1, 2, 3, 4, 0, 1, 2, 3, 4];
///
/// // WHEN
/// let actual = ArrayOfBase::<10, 5>::try_new(array);
///
/// // THEN
/// assert!(actual.is_ok())
/// ```
///
/// # Example - Error
///
/// ```
/// # use check_hash_calculator::data_object::ArrayOfBase;
/// // GIVEN
/// let array = [16];
///
/// // WHEN
/// let actual = ArrayOfBase::<1, 16>::try_new(array);
///
/// // THEN
/// assert!(actual.is_err())
/// ```
// #[wasm_bindgen::prelude::wasm_bindgen]
#[derive(PartialEq, Eq)]
pub struct ArrayOfBase<const N: usize, const B: u8> {
    value: [u8; N],
}

/// Failure state for constructing [`ArrayOfBase`]
///
/// # Used in
/// - [`ArrayOfBase::try_new`],
/// - [`ArrayOfBase::try_from_vec_exact`]
/// - [`ArrayOfBase::try_from_vec_pad`]
#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    /// Supplied array is larger then specified `N`
    Oversized,
    /// Supplied array is smaller then specified `N`
    ///
    /// Note: Not returned in [`ArrayOfBase::try_from_vec_pad`] as any non-zero
    /// size smaller than `N` is accepted
    Undersized,
    /// Supplied array is empty
    Empty,
    /// Supplied array contains a value equal-to or larger than `B`
    InvalidValue,
}

impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}

impl<const N: usize, const B: u8> ArrayOfBase<N, B> {
    /// Create from Array of u8 numbers
    ///
    /// See [`ArrayOfBase`] for Example
    ///
    /// # Errors
    ///
    /// // TODO 1666978255
    pub fn try_new(v: [u8; N]) -> Result<Self, Error> {
        if v.iter().copied().all(Self::valid_number) {
            Ok(Self { value: v })
        } else {
            Err(Error::InvalidValue)
        }
    }

    ///
    ///
    /// # Errors
    ///
    /// // TODO 1666978271
    pub fn try_from_vec_exact(v: &[u8]) -> Result<Self, Error> {
        use std::cmp::Ordering::{Equal, Greater, Less};
        match v.len().cmp(&N) {
            Greater => Err(Error::Oversized),
            Less if v.is_empty() => Err(Error::Empty),
            Less => Err(Error::Undersized),
            Equal => {
                Self::try_new(v.try_into().expect("1666898415 - Unreachable"))
            }
        }
    }

    ///
    ///
    /// # Errors
    ///
    /// // TODO 1666978279
    ///
    /// # Panics
    ///
    /// // TODO 1666978304
    pub fn try_from_vec_pad(v: &[u8]) -> Result<Self, Error> {
        use std::cmp::Ordering::{Greater, Less};
        match v.len().cmp(&N) {
            Greater => Err(Error::Oversized),
            Less if v.is_empty() => Err(Error::Empty),
            _ => Self::try_new(Self::pad_array(v)),
        }
    }

    /// Return the contained verified array
    ///
    /// # Example
    ///
    /// ```
    /// # use check_hash_calculator::data_object::ArrayOfBase;
    /// // GIVEN
    /// # let valid_u8_array = [1, 2, 3];
    /// # let mut verified = ArrayOfBase::<3, 10>::default();
    /// verified = ArrayOfBase::try_new(valid_u8_array).unwrap();
    ///
    /// // WHEN
    /// let actual = verified.unwrap();
    ///
    /// // THEN
    /// assert_eq!(actual, valid_u8_array)
    /// ```
    #[must_use]
    pub fn unwrap(self) -> [u8; N] {
        self.value
    }

    /// Borrow the contained verified array
    ///
    /// # Example
    ///
    /// ```
    /// # use check_hash_calculator::data_object::ArrayOfBase;
    /// // GIVEN
    /// # let valid_u8_array = [1, 2, 3];
    /// # let mut verified = ArrayOfBase::<3, 10>::default();
    /// verified = ArrayOfBase::try_new(valid_u8_array).unwrap();
    ///
    /// // WHEN
    /// let actual = verified.borrow_u8_array();
    ///
    /// // THEN
    /// assert_eq!(actual, &valid_u8_array)
    /// ```
    #[must_use]
    pub fn borrow_u8_array(&self) -> &[u8; N] {
        &self.value
    }

    fn valid_number(u8: u8) -> bool {
        u8 < B
    }

    /// Prepend to array 0 values until `N` size reached
    ///
    /// # Panics
    ///
    /// Will Panic if an array that exceeds the desired length is passed
    ///
    /// # Example
    ///
    /// ```
    /// # use check_hash_calculator::data_object::ArrayOfBase;
    /// // GIVEN
    /// let x = [1, 2, 3];
    ///
    /// // WHEN
    /// let actual = ArrayOfBase::<5, 10>::pad_array(&x);
    ///
    /// // THEN
    /// assert_eq!(actual, [0u8, 0, 1, 2, 3]);
    /// ```
    #[must_use]
    pub fn pad_array(v: &[u8]) -> [u8; N] {
        assert!(v.len() <= N);
        let mut vec = v.to_vec();
        while vec.len() < N {
            vec.insert(0, 0);
        }
        vec.try_into().expect("1666979421 - Unreachable")
    }
}

impl<const N: usize, const B: u8> Default for ArrayOfBase<N, B> {
    fn default() -> Self {
        Self { value: [0u8; N] }
    }
}
