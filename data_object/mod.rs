//! Struct, Enum, or Type containing data and validation ONLY, no business logic

pub mod array_of_base;

pub use array_of_base::ArrayOfBase;

/// 9-digit number representing a Financial Institution
///
/// This information is publicly available and a limited number of
/// [`RoutingNumber`] are assigned to each
/// [Financial Institution](../index.html#financial-institution)
///
/// [`RoutingNumber`] is stored as 9 decimal values stored as a fixed
/// 9-element array of [u8] validated using [`ArrayOfBase`]
///
/// # Example
///
/// ```
/// # use check_hash_calculator::data_object::RoutingNumber;
/// assert!(
///     RoutingNumber::try_new([1u8, 2, 3, 4, 5, 6, 7, 8, 9]).is_ok()
/// )
/// ```
pub type RoutingNumber = ArrayOfBase<9, 10>;

/// 12-digit number representing a specific account at a Financial Institution
///
/// [`AccountNumber`] is issued by
/// [Financial Institution](../index.html#financial-institution) and are usually
/// sequentially issued
///
/// [`AccountNumber`] may be less than 12-digits long, and are pre-padded by
/// `0`.
///
/// [`AccountNumber`] is stored as 12 decimal values stored as a fixed
/// 12-element array of [u8] validated using [`ArrayOfBase`].
///
/// # Example
///
/// ```
/// # use check_hash_calculator::data_object::AccountNumber;
/// assert!(
///     AccountNumber::try_from_vec_pad(&[1u8, 2, 3]).unwrap().unwrap()
///     == [0u8, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3]
/// )
/// ```
pub type AccountNumber = ArrayOfBase<12, 10>;

/// Random 256 bit value used for security
///
/// Article: <https://en.wikipedia.org/wiki/Pepper_(cryptography)>
///
/// A [`Pepper`] is fixed and known **ONLY** by [Client](../index.html#client)
/// to ensure [`CheckHash`][CheckHash] is not guessable by
/// [Unintended 3rd Party](../index.html#unintended-3rd-party)
///
/// The [`Pepper`] is stored as 16 hexadecimal values stored as a fixed
/// 16-element array of [u8] validated using [`ArrayOfBase`].
///
/// # Example
///
/// ```
/// # use check_hash_calculator::data_object::Pepper;
/// assert!(
///     Pepper::try_from_vec_exact(
///         &[10u8, 13, 3, 8, 1, 5, 15, 8, 4, 3, 13, 1, 7, 15, 8, 12]
///     ).is_ok()
/// )
/// ```
pub type Pepper = ArrayOfBase<16, 16>;

/// 256 bit number calculated from [`CheckMetadata`] and [`Pepper`]
///
/// Article: <https://en.wikipedia.org/wiki/Hash_function>
///
/// Each [`CheckMetadata`] will calculate to the same [`CheckHash`] assuming
/// the same [`Pepper`] is used.  It is computationally prohibitive (if not
/// impossible) for [Unintended 3rd Party](../index.html#unintended-3rd-party)
/// to determine [`CheckMetadata`] from [`CheckHash`] (assuming [`Pepper`]
/// is unknown by [Unintended 3rd Party](../index.html#unintended-3rd-party))
pub type CheckHash = ArrayOfBase<7, 16>;

/// [`RoutingNumber`] and [`AccountNumber`]
pub struct CheckMetadata {
    pub routing_number: RoutingNumber,
    pub account_number: AccountNumber,
}
