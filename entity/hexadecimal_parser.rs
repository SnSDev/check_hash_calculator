//! Parse a hexadecimal number from string

use crate::entity::NumberParser;

pub type Error = crate::data_object::array_of_base::Error;

/// Parse a hexadecimal number from string
///
/// # Example
///
/// ```
/// # use check_hash_calculator::entity::HexadecimalParser;
/// # use check_hash_calculator::data_object::ArrayOfBase;
/// // GIVEN
/// let string = "abcdef0123456789ABCDEF".to_owned();
///
/// // WHEN
/// let actual = HexadecimalParser::<22>::parse(&string);
///
/// // THEN
/// assert!(
///     actual.ok().unwrap()
///         == ArrayOfBase::<22, 16>::try_new([
///             10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
///             11, 12, 13, 14, 15
///         ])
///         .unwrap()
/// );
/// ```
pub type HexadecimalParser<const N: usize> = NumberParser<N, 16>;
