//! Use Case independent business logic

pub mod decimal_parser;
pub mod hexadecimal_parser;
pub mod number_parser;

pub use decimal_parser::DecimalParser;
pub use hexadecimal_parser::HexadecimalParser;
pub use number_parser::NumberParser;
