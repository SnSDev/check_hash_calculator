//! Parse string into respective [`ArrayOfBase`]

use crate::data_object::{
    array_of_base::Error as ArrayOfBaseError, ArrayOfBase,
};

pub type Error = ArrayOfBaseError;

pub struct NumberParser<const N: usize, const B: u8> {}

impl<const N: usize, const B: u8> NumberParser<N, B> {
    /// Parse string into respective [`ArrayOfBase`]
    ///
    /// Used By:
    /// - [`crate::entity::DecimalParser`]
    /// - [`crate::entity::HexadecimalParser`]
    ///
    /// # Errors
    ///
    /// Since [`ArrayOfBase::try_from_vec_exact`] is run at the end,
    /// any errors that function may produce are passed through.
    ///
    /// Also [`Error::InvalidValue`] is returned if an unrecognized character
    /// is in the string.
    ///
    pub fn parse(s: &str) -> Result<ArrayOfBase<N, B>, Error> {
        ArrayOfBase::try_from_vec_exact(&string_to_u8_vec(s)?)
    }

    /// Parse string into respective [`ArrayOfBase`] with '0's padded
    ///
    /// Used By:
    /// - [`crate::entity::DecimalParser`]
    /// - [`crate::entity::HexadecimalParser`]
    ///
    /// # Errors
    ///
    /// Since [`ArrayOfBase::try_from_vec_pad`] is run at the end,
    /// any errors that function may produce are passed through.
    ///
    /// Also [`Error::InvalidValue`] is returned if an unrecognized character
    /// is in the string.
    ///
    /// # Example
    ///
    /// ```
    /// # use check_hash_calculator::entity::NumberParser;
    /// # use check_hash_calculator::data_object::ArrayOfBase;
    /// // GIVEN
    /// let string = "123".to_owned();
    ///
    /// // WHEN
    /// let actual = NumberParser::<6, 4>::parse_pad(&string);
    ///
    /// // THEN
    /// assert!(
    ///     actual.ok().unwrap()
    ///         == ArrayOfBase::<6, 4>::try_new([
    ///             0, 0, 0, 1, 2, 3
    ///         ])
    ///         .unwrap()
    /// );
    /// ```
    pub fn parse_pad(s: &str) -> Result<ArrayOfBase<N, B>, Error> {
        ArrayOfBase::try_from_vec_pad(&string_to_u8_vec(s)?)
    }
}

fn string_to_u8_vec(s: &str) -> Result<Vec<u8>, Error> {
    let mut num_vec = Vec::new();
    for c in s.chars() {
        num_vec.push(num_from_char(c)?);
    }
    Ok(num_vec)
}

fn num_from_char(c: char) -> Result<u8, Error> {
    match c {
        '0' => Ok(0),
        '1' => Ok(1),
        '2' => Ok(2),
        '3' => Ok(3),
        '4' => Ok(4),
        '5' => Ok(5),
        '6' => Ok(6),
        '7' => Ok(7),
        '8' => Ok(8),
        '9' => Ok(9),
        'a' | 'A' => Ok(10),
        'b' | 'B' => Ok(11),
        'c' | 'C' => Ok(12),
        'd' | 'D' => Ok(13),
        'e' | 'E' => Ok(14),
        'f' | 'F' => Ok(15),
        _ => Err(Error::InvalidValue),
    }
}
