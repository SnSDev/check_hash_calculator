//! Parse a decimal number from string
use crate::entity::NumberParser;

pub type Error = crate::data_object::array_of_base::Error;

/// Parse a decimal number from string
///
/// # Example
///
/// ```
/// # use check_hash_calculator::entity::{DecimalParser, decimal_parser::Error};
/// # use check_hash_calculator::data_object::{ArrayOfBase};
/// // GIVEN
/// let string = "0123456789a".to_owned();
///
/// // WHEN
/// let actual = DecimalParser::<11>::parse(&string);
///
/// // THEN
/// assert!(actual.err().unwrap() == Error::InvalidValue);
/// ```
pub type DecimalParser<const N: usize> = NumberParser<N, 10>;
