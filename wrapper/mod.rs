//! Plug-In for external Crate implementing local Trait

#[cfg(feature = "rand")]
mod random_hex_rand;

#[cfg(feature = "rand")]
pub use random_hex_rand::RandomHexRand;
