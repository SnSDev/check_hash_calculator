use crate::generate_pepper_use_case::RandomHex;
use rand::{rngs::ThreadRng, Rng};

/// Implement [`RandomHexRand`] for [`rand`] crate
///
/// # Example
///
/// ```
/// # use check_hash_calculator::wrapper::RandomHexRand;
/// # use check_hash_calculator::generate_pepper_use_case::RandomHex;
/// // GIVEN
/// let mut randomizer = RandomHexRand::default();
/// let mut array = [true; 16];
/// let mut too_many_tries = 0;
///
/// // WHEN
/// while array.iter().any(|v| *v) {
///     let val = randomizer.rand_hex();
///     array[val as usize] = false;
///     too_many_tries += 1;
///     assert!(too_many_tries < 100);
/// }
///
/// // THEN
/// assert!(true); // All values were found in random search
///                // Note: It is technically possible for this test to fail
///                // but nothing be wrong.  It is just highly unlikely.
/// ```
pub struct RandomHexRand {
    rng: ThreadRng,
}

impl Default for RandomHexRand {
    fn default() -> Self {
        Self {
            rng: rand::thread_rng(),
        }
    }
}

impl RandomHex for RandomHexRand {
    fn rand_hex(&mut self) -> u8 {
        self.rng.gen_range(0..16)
    }
}
