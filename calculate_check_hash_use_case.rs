//! Calculate the hash for the supplied check metadata

use crate::data_object::{
    AccountNumber, ArrayOfBase, CheckHash, CheckMetadata, Pepper, RoutingNumber,
};
use sha2::{Digest, Sha256};

pub mod prelude {
    pub use super::*;
}

pub type Result = std::result::Result<ResponseModel, Error>;

/// Calculate the hash for the supplied check metadata
///
/// # Errors
///
/// There are no error states for this use case.
///
/// # Example
///
/// ```
/// # use check_hash_calculator::data_object::{
/// #   RoutingNumber, AccountNumber, Pepper
/// # };
/// # use check_hash_calculator::calculate_check_hash_use_case::prelude::*;
/// // GIVEN
/// let routing_number = [1, 2, 3, 4, 5, 6, 7, 8, 9];
/// let account_number = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2];
/// let pepper = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
/// let request_model = RequestModel::new(
///     RoutingNumber::try_new(routing_number).unwrap(),
///     AccountNumber::try_new(account_number).unwrap(),
///     Pepper::try_new(pepper).unwrap(),
/// );
///
/// // WHEN
/// let actual = calculate_check_hash(request_model);
///
/// // THEN
/// assert_eq!(
///     actual.ok().unwrap().check_hash.unwrap(),
///     [2u8, 15, 14, 13, 2, 14, 0]
/// )
/// ```
///
/// # Panics
///
/// // TODO: Remove
///
pub fn calculate_check_hash(request_model: RequestModel) -> Result {
    let mut hasher = Sha256::new();
    hasher.update(request_model.into_u8_array());
    let check_hash: [u8; 32] = hasher.finalize().into();

    Ok(ResponseModel {
        check_hash: ArrayOfBase::try_new(
            check_hash
                .iter()
                .flat_map(u8_to_u4_as_u8)
                .take(7)
                .collect::<Vec<u8>>()
                .try_into()
                .expect("1668124467 - Unreachable"),
        )
        .expect("1668125561 - Unreachable"),
    })
}
/// Convert u8 into 2 u4 encoded as u8
///
/// # Example
///
/// ```
/// # use check_hash_calculator::calculate_check_hash_use_case::u8_to_u4_as_u8;
/// // GIVEN
/// let x = 0xab as u8;
///
/// // WHEN
/// let actual = u8_to_u4_as_u8(&x);
///
/// // THEN
///
/// assert_eq!(actual, [0xa as u8, 0xb])
/// ```
#[must_use]
pub fn u8_to_u4_as_u8(v: &u8) -> [u8; 2] {
    [(v & 0xf0) >> 4, v & 0x0f]
}

pub struct RequestModel {
    pub check_metadata: CheckMetadata,
    pub pepper: Pepper,
}

impl From<RequestModel> for [u8; 37] {
    /// Concatenate contained numbers into u8 array
    ///
    /// # Example
    ///
    /// ```
    /// # use check_hash_calculator::data_object::{
    /// #   RoutingNumber, AccountNumber, Pepper
    /// # };
    /// # use check_hash_calculator::calculate_check_hash_use_case::prelude::*;
    /// // GIVEN
    /// let routing_number = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    /// let account_number = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2];
    /// let pepper = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
    /// let request_model = RequestModel::new(
    ///     RoutingNumber::try_new(routing_number).unwrap(),
    ///     AccountNumber::try_new(account_number).unwrap(),
    ///     Pepper::try_new(pepper).unwrap(),
    /// );
    ///
    /// // WHEN
    /// let actual = request_model.into_u8_array();
    ///
    /// // THEN
    /// assert_eq!(
    ///     actual,
    ///     [
    ///         1u8, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1,
    ///         2, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
    ///     ]
    /// )
    /// ```
    fn from(
        RequestModel {
            check_metadata:
                CheckMetadata {
                    routing_number,
                    account_number,
                },
            pepper,
        }: RequestModel,
    ) -> Self {
        let mut iter = { [].iter() }
            .chain(routing_number.borrow_u8_array())
            .chain(account_number.borrow_u8_array())
            .chain(pepper.borrow_u8_array());
        core::array::from_fn(|_| *iter.next().unwrap())
    }
}

impl RequestModel {
    #[must_use]
    pub fn into_u8_array(self) -> [u8; 37] {
        self.into()
    }

    #[must_use]
    pub fn new(
        routing_number: RoutingNumber,
        account_number: AccountNumber,
        pepper: Pepper,
    ) -> Self {
        Self {
            check_metadata: CheckMetadata {
                routing_number,
                account_number,
            },
            pepper,
        }
    }
}

#[derive(PartialEq, Eq)]
pub struct ResponseModel {
    pub check_hash: CheckHash,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {}
