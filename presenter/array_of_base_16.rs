//! Render Hexadecimal [`ArrayOfBase`] as [`String`]
//!
//! # Example
//!
//! ```
//! # use check_hash_calculator::data_object::ArrayOfBase;
//! # use check_hash_calculator::presenter::array_of_base_16::prelude::*;
//! // GIVEN
//! let value: ArrayOfBase<16, 16> =
//!     ArrayOfBase::try_new(core::array::from_fn(|i| i as u8)).unwrap();
//! let presenter = Presenter::default();
//!
//! // WHEN
//! let actual: String = presenter.render(&value).to_string();
//!
//! // THEN
//! assert_eq!(actual, "0123456789abcdef");
//! ```

use crate::data_object::ArrayOfBase;

pub mod prelude {
    pub use super::*;
}

/// Settings that change the behavior of [`Display`]
///
/// No settings in-use for this presenter
#[derive(Default)]
pub struct Presenter<const N: usize> {
    // No settings at this time
}

impl<const N: usize> Presenter<N> {
    #[must_use]
    pub fn render<'a>(
        &'a self,
        value: &'a ArrayOfBase<N, 16>,
    ) -> Display<'a, N> {
        Display {
            value,
            _settings: self, // No settings at this time
        }
    }
}

/// Wrapper for [`ArrayOfBase`] implementing [`std::fmt::Display`]
pub struct Display<'a, const N: usize> {
    value: &'a ArrayOfBase<N, 16>,
    _settings: &'a Presenter<N>,
}

impl<'a, const N: usize> std::fmt::Display for Display<'a, N> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for v in self.value.borrow_u8_array().iter() {
            match v {
                10 => write!(f, "a")?,
                11 => write!(f, "b")?,
                12 => write!(f, "c")?,
                13 => write!(f, "d")?,
                14 => write!(f, "e")?,
                15 => write!(f, "f")?,
                v => write!(f, "{v}")?,
            }
        }
        Ok(())
    }
}
