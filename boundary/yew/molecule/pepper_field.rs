use crate::atom::{
    yew::{MAINTAIN_COMPONENT, RE_RENDER_COMPONENT},
    GeneratePepperButtonGroupItem,
};
use crate::emeraldinspirations_theme::atom::{
    FieldHelp, FieldLabel, FieldValidity as GenFieldValidity,
};
use crate::molecule::PrivateFieldSubGroup;
use yew::prelude::*;

pub struct PepperField {}

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum FieldValidity {
    Empty,
    TooLong { size: usize },
    TooShort { size: usize },
    Invalid,
    Valid,
}

impl Default for FieldValidity {
    fn default() -> Self {
        Self::Empty
    }
}

impl From<FieldValidity> for GenFieldValidity {
    fn from(value: FieldValidity) -> Self {
        match value {
            FieldValidity::Empty | FieldValidity::TooShort { size: _ } => {
                Self::Empty
            }
            FieldValidity::Valid => Self::Valid,
            _ => Self::Invalid,
        }
    }
}

impl FieldValidity {
    pub fn render_help(&self) -> String {
        match self {
            Self::Valid => "Valid".to_owned(),
            Self::TooLong { size } => {
                format!(
                    "Too Long: {size} hexadecimal digits provided, 16 expected"
                )
            }
            Self::TooShort { size } => {
                format!(
                    "Too Short: {size} hexadecimal digits provided, 16 \
                     expected"
                )
            }
            Self::Invalid => "Unrecognized Digit: Numbers 0-9 and/or letters \
                              a-f expected"
                .to_owned(),
            Self::Empty => "0 of 16 hexadecimal digits".to_owned(),
        }
    }
}

#[derive(Clone)]
pub enum Message {
    GeneratePepper,
    UpdatePepper(String),
}

#[derive(PartialEq, yew::Properties)]
pub struct Properties {
    pub callback: Callback<Message>, // Can't derive Eq
    pub valid: FieldValidity,
    pub value: String,
}

impl Component for PepperField {
    type Message = Message;
    type Properties = Properties;

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self {}
    }

    fn update(&mut self, ctx: &yew::Context<Self>, msg: Self::Message) -> bool {
        ctx.props().callback.emit(msg.clone());
        match msg {
            Message::GeneratePepper => RE_RENDER_COMPONENT,
            Message::UpdatePepper(_) => MAINTAIN_COMPONENT,
        }
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        let link = ctx.link();

        let generate_pepper_button_on_click: Callback<()> =
            link.callback(|_| Message::GeneratePepper);
        let update_pepper: Callback<String> =
            link.callback(Message::UpdatePepper);
        let valid = GenFieldValidity::from(ctx.props().valid);

        html!(
            <div class="mb-3">
                <FieldLabel field_name={FIELD_NAME} display={ "Pepper" } />
                <div class="input-group">
                    <PrivateFieldSubGroup
                        value={ctx.props().value.clone()}
                        valid={valid}
                        name={FIELD_NAME}
                        on_change={update_pepper} />
                    <GeneratePepperButtonGroupItem
                        on_click={generate_pepper_button_on_click}
                        valid={valid}
                    />
                </div>
                <FieldHelp
                    valid={valid}
                    display={ ctx.props().valid.render_help() }
                />
            </div>
        )
        // placeholder="Recipient's username"
        // aria-label="Recipient's username"
        // aria-describedby="button-addon2"
    }
}

const FIELD_NAME: &str = "PepperField";
