use crate::emeraldinspirations_theme::atom::AppLogo;
use crate::molecule::AppInstallStatus;
use yew::prelude::*;

#[derive(Default)]
pub struct AppCardHeader {}

impl Component for AppCardHeader {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html!(
            <div class="card-header">
                <AppLogo />
                { "Check Hash Calculator" }
                <AppInstallStatus />
            </div>
        )
    }
}
