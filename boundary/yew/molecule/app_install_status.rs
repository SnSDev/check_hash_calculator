use crate::atom::{
    cache_state_link::{CacheState, Message as CacheStateMessage},
    CacheStateLink,
};
use crate::emeraldinspirations_theme::atom::WebAppStatus;
use wasm_bindgen::{JsCast, JsValue};
use wasm_bindgen_futures::JsFuture;
use yew::prelude::*;

const CACHE_NAME: &str = "check_hash_calculator";

const FILES: [&str; 17] = [
    "./",
    "./bootstrap-3b5f49edee3b6a90.css",
    "./bootstrap-icons-44e1655fb3e5314.css",
    "./bootstrap-icons.woff",
    "./bootstrap-icons.woff2",
    "./check_hash_calculator_yew_app-e69e0776332eec4a_bg.wasm",
    "./check_hash_calculator_yew_app-e69e0776332eec4a.js",
    "./favicon.ico",
    "./icon-192x192.png",
    "./icon-256x256.png",
    "./icon-384x384.png",
    "./icon-512x512.png",
    "./index.html",
    "./manifest.json",
    "./maskable_icon.png",
    "./service_worker.js",
    "./style-c920ca43256fdcb9.css",
];

#[derive(Default)]
pub enum Resource<T> {
    #[default]
    None,
    Ok(T),
    Err,
}

impl<T> Resource<T> {
    pub fn borrow(&self) -> &T {
        match self {
            Self::Ok(v) => v,
            _ => panic!("1668624795 - Borrow on non-Ok state"),
        }
    }

    pub fn is_err(&self) -> bool {
        matches!(self, Self::Err)
    }
}

#[derive(Default)]
pub struct AppInstallStatus {
    pub window: Resource<web_sys::Window>,
    pub cache_storage: Resource<web_sys::CacheStorage>,
    pub cache_status: CacheState,
}

pub enum Message {
    CacheStatusUpdated(CacheState),
    UpdateCacheStatus,
    DeleteCache,
    CacheApp,
}

impl From<CacheStateMessage> for Message {
    fn from(value: CacheStateMessage) -> Self {
        match value {
            CacheStateMessage::CacheApp => Self::CacheApp,
            CacheStateMessage::DeleteCache => Self::DeleteCache,
        }
    }
}

#[derive(PartialEq, Properties)]
pub struct Properties {}

impl Component for AppInstallStatus {
    type Message = Message;
    type Properties = Properties;

    fn create(ctx: &Context<Self>) -> Self {
        let mut s = Self::default();

        load_window(&mut s);
        if s.window.is_err() {
            return s;
        }

        load_cache_storage(&mut s);
        if s.cache_storage.is_err() {
            return s;
        }

        ctx.link().send_message(Message::UpdateCacheStatus);

        s
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Message::UpdateCacheStatus => {
                let cache_storage_clone = self.cache_storage.borrow().clone();

                ctx.link().send_future(async move {
                    match verify_cache_exists(cache_storage_clone).await {
                        Ok(v) if v.as_bool() == Some(true) => {
                            Message::CacheStatusUpdated(CacheState::Present)
                        }
                        Ok(_) => Message::CacheStatusUpdated(CacheState::Empty),
                        Err(_e) => Message::CacheStatusUpdated(
                            CacheState::UnableToDetermine,
                        ),
                    }
                });
                ctx.link().send_message(Message::CacheStatusUpdated(
                    CacheState::Verifying,
                ));
            }
            Message::CacheStatusUpdated(v) => self.cache_status = v,
            Message::CacheApp => {
                let cache_storage_clone = self.cache_storage.borrow().clone();
                ctx.link().send_future(async move {
                    match cache_app(cache_storage_clone).await {
                        Ok(_) => Message::UpdateCacheStatus,
                        Err(_) => Message::CacheStatusUpdated(
                            CacheState::UnableToFill,
                        ),
                    }
                });
            }
            Message::DeleteCache => {
                let cache_storage_clone = self.cache_storage.borrow().clone();
                ctx.link().send_future(async move {
                    let _result = delete_cache(cache_storage_clone).await;
                    Message::UpdateCacheStatus
                });
            }
        }
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let callback = link.callback(Message::from);

        html!(
            <div style="float:right;">
                <WebAppStatus />
                { " " }
                <CacheStateLink
                    state={self.cache_status}
                    callback={callback}
                />
            </div>
        )
    }
}

fn load_window(state: &mut AppInstallStatus) {
    state.window = if let Some(window) = web_sys::window() {
        Resource::Ok(window)
    } else {
        Resource::Err
    };
}

fn load_cache_storage(state: &mut AppInstallStatus) {
    state.cache_storage =
        if let Ok(cache_storage) = state.window.borrow().caches() {
            Resource::Ok(cache_storage)
        } else {
            Resource::Err
        };
}

async fn verify_cache_exists(
    cache_storage: web_sys::CacheStorage,
) -> Result<JsValue, JsValue> {
    JsFuture::from(cache_storage.has(CACHE_NAME)).await
}

async fn delete_cache(
    cache_storage: web_sys::CacheStorage,
) -> Result<JsValue, JsValue> {
    JsFuture::from(cache_storage.delete(CACHE_NAME)).await
}

async fn cache_app(
    cache_storage: web_sys::CacheStorage,
) -> Result<JsValue, JsValue> {
    let cache = { JsFuture::from(cache_storage.open(CACHE_NAME)).await? }
        .dyn_into::<web_sys::Cache>()?;

    JsFuture::from(
        cache.add_all_with_str_sequence(
            &{ FILES.into_iter() }
                .map(JsValue::from_str)
                .collect::<js_sys::Array>(),
        ),
    )
    .await
}

// /* Start the service worker and cache all of the app's content */
// self.addEventListener("install", function (e) {
//     e.waitUntil(
//         caches.open(cacheName).then(function (cache) {
//             return cache.addAll(filesToCache);
//         })
//     );
// });
