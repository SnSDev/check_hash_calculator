use crate::atom::yew::RE_RENDER_COMPONENT;
use crate::emeraldinspirations_theme::atom::{
    FieldValidity, PrivateFieldMask, PrivateInputBox, ToggleMaskButton,
};
use yew::prelude::*;

#[derive(Default)]
pub struct PrivateFieldSubGroup {
    mask: PrivateFieldMask,
}

pub enum Message {
    ToggleMask,
}

#[derive(PartialEq, yew::Properties)]
pub struct Properties {
    pub name: String,
    pub value: String,
    pub valid: FieldValidity,
    pub on_change: Callback<String>, // Can't derive Eq
}

impl Component for PrivateFieldSubGroup {
    type Message = Message;
    type Properties = Properties;

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self::default()
    }

    fn update(
        &mut self,
        _ctx: &yew::Context<Self>,
        msg: Self::Message,
    ) -> bool {
        match msg {
            Message::ToggleMask => {
                self.mask.toggle();
                RE_RENDER_COMPONENT
            }
        }
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        let link = ctx.link();
        let on_click: Callback<()> = link.callback(|_| Message::ToggleMask);

        html!(
        <>
            <ToggleMaskButton
                mask={self.mask}
                on_click={on_click}
                valid={ctx.props().valid}
            />

            <PrivateInputBox
                mask={self.mask}
                name={ctx.props().name.clone()}
                on_change={ctx.props().on_change.clone()}
                valid={ctx.props().valid}
                value={ctx.props().value.clone()}
            />
        </>
        )
    }
}
