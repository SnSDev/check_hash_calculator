use crate::atom::bootstrap::Color;
use yew::prelude::*;

#[derive(Default)]
pub struct ExampleCheckBar {}

pub enum Message {
    ExampleCheck1,
    ExampleCheck2,
    ExampleCheck3,
    ExampleCheck4,
    ExampleCheck5,
}

#[derive(PartialEq, yew::Properties)]
pub struct Properties {
    pub callback: Callback<Message>,
}

impl Component for ExampleCheckBar {
    type Message = Message;
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        ctx.props().callback.emit(msg);
        false
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();

        html!(
            <div
              class="btn-group btn-group-sm"
              role="group"
              aria-label="Basic outlined example"
              style="overflow-x:auto;width:100%;"
            >
              <button
                type="button"
                class={Color::Info.button_outline().to_string()}
                onclick={link.callback(|_| Message::ExampleCheck1)}
              >{"Example Check #1"}</button>
              <button
                type="button"
                class={Color::Info.button_outline().to_string()}
                onclick={link.callback(|_| Message::ExampleCheck2)}
              >{"Example Check #2"}</button>
              <button
                type="button"
                class={Color::Info.button_outline().to_string()}
                onclick={link.callback(|_| Message::ExampleCheck3)}
              >{"Example Check #3"}</button>
              <button
                type="button"
                class={Color::Info.button_outline().to_string()}
                onclick={link.callback(|_| Message::ExampleCheck4)}
              >{"Example Check #4"}</button>
              <button
                type="button"
                class={Color::Info.button_outline().to_string()}
                onclick={link.callback(|_| Message::ExampleCheck5)}
              >{"Example Check #5"}</button>
            </div>
        )
    }
}
