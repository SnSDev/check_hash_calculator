use crate::emeraldinspirations_theme::atom::FieldLabel;
use yew::prelude::*;

const FIELD_NAME: &str = "CheckHashField";

#[derive(Default)]
pub struct CheckHashField {}

#[derive(PartialEq, Properties)]
pub struct Properties {
    pub value: String,
}

impl Component for CheckHashField {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn update(&mut self, _ctx: &Context<Self>, _msg: Self::Message) -> bool {
        false
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html!(
            <div class="mb-3">
                <FieldLabel
                    field_name={FIELD_NAME}
                    display={"Check Hash"}
                />
                <div class="input-group">
                    <input
                        value={ctx.props().value.clone()}
                        readonly={true}
                        id={FIELD_NAME}
                        class="form-control"
                    />
                </div>
            </div>
        )
    }
}
