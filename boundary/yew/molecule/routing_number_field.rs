use crate::emeraldinspirations_theme::atom::{
    FieldHelp, FieldLabel, FieldValidity as GenFieldValidity,
};
use crate::molecule::PrivateFieldSubGroup;
use yew::prelude::*;

const FIELD_NAME: &str = "RoutingNumberField";
const EXPECTED_SIZE: usize = 9;
pub const EXAMPLE: &str = "110000000";

#[derive(Default)]
pub struct RoutingNumberField {}

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum FieldValidity {
    Empty,
    TooLong { size: usize },
    TooShort { size: usize },
    Invalid,
    Valid,
}

impl Default for FieldValidity {
    fn default() -> Self {
        Self::Empty
    }
}

impl From<FieldValidity> for GenFieldValidity {
    fn from(value: FieldValidity) -> Self {
        match value {
            FieldValidity::Empty | FieldValidity::TooShort { size: _ } => {
                Self::Empty
            }
            FieldValidity::Valid => Self::Valid,
            _ => Self::Invalid,
        }
    }
}

impl FieldValidity {
    pub fn render_help(&self) -> String {
        match self {
            Self::Valid => "Valid".to_owned(),
            Self::TooShort { size } => {
                format!(
                    "Too Short: {size} digits provided, {EXPECTED_SIZE} \
                     expected"
                )
            }
            Self::TooLong { size } => {
                format!(
                    "Too Long: {size} digits provided, {EXPECTED_SIZE} \
                     expected"
                )
            }
            Self::Invalid => {
                "Unrecognized Digit: Numbers 0-9 expected".to_owned()
            }
            Self::Empty => format!("0 of {EXPECTED_SIZE} digits"),
        }
    }
}

#[derive(PartialEq, Properties)]
pub struct Properties {
    pub callback: Callback<Message>,
    pub valid: FieldValidity,
    pub value: String,
}

#[derive(Clone)]
pub enum Message {
    Update(String),
}

impl Component for RoutingNumberField {
    type Message = Message;
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        ctx.props().callback.emit(msg);
        false
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();

        let on_change: Callback<String> = link.callback(Message::Update);

        let gen_valid = GenFieldValidity::from(ctx.props().valid);

        html!(
            <div class="mb-3">
                <FieldLabel
                    field_name={FIELD_NAME}
                    display={"Routing Number"}
                />
                <div class="input-group">
                    <PrivateFieldSubGroup
                        value={ctx.props().value.clone()}
                        valid={gen_valid}
                        name={FIELD_NAME}
                        on_change={on_change}
                    />
                </div>
                <FieldHelp
                    valid={gen_valid}
                    display={ctx.props().valid.render_help()}
                />
            </div>
        )
    }
}
