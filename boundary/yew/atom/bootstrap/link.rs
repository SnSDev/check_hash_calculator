use crate::atom::html::{Button as HtmlButton, Class, Style, TextDecoration};
use yew::prelude::*;

#[derive(Default)]
pub struct Link {}

#[derive(PartialEq, Properties)]
pub struct Properties {
    #[prop_or_default]
    pub children: Children,

    pub text_decoration: Option<TextDecoration>,
    pub on_click: Option<Callback<MouseEvent>>,
}

impl Component for Link {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let style = Style::default()
            .with_style_option(ctx.props().text_decoration.map(Into::into))
            .with("margin", "0")
            .with("padding", "0");

        let on_click = ctx
            .props()
            .on_click
            .clone()
            .unwrap_or_else(|| Callback::from(|_| ()));

        let class = Class::default().with("btn").with("btn-link");

        html!(
            <HtmlButton
                class={class}
                style={style}
                on_click={on_click}
            >
                {ctx.props().children.clone()}
            </HtmlButton>
        )
    }
}
