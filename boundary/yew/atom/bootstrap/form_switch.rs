use yew::prelude::*;

#[derive(Default)]
pub struct FormSwitch {}

#[derive(PartialEq, Properties)]
pub struct Properties {
    pub name: String,
    pub label: String,
}

impl Component for FormSwitch {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html!(
            <div class="form-check form-check-inline form-switch">
                <input
                    class="form-check-input"
                    type="checkbox"
                    role="switch"
                    id={ ctx.props().name.clone() }
                />
                <label
                    class="form-check-label"
                    for={ ctx.props().name.clone() }
                >
                    { ctx.props().label.clone() }
                </label>
            </div>
        )
    }
}
