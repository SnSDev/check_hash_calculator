use yew::prelude::*;

#[derive(Default)]
pub struct Badge {}

#[derive(PartialEq, Properties)]
pub struct Properties {
    pub caption: String,
}

impl Component for Badge {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html!(
            <span class="position-absolute top-0 start-100 translate-middle p-2 bg-danger border border-light rounded-circle">
                <span class="visually-hidden">{ ctx.props().caption.clone() }</span>
            </span>
        )
    }
}
