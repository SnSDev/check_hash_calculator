use crate::atom::{
    bootstrap::Color,
    html::{Class, Style},
};
use yew::prelude::*;

#[derive(Default)]
pub struct Alert {}

#[derive(PartialEq, Properties)]
pub struct Properties {
    #[prop_or_default]
    pub children: Children,

    #[prop_or_default]
    pub class: Class,

    #[prop_or_default]
    pub style: Style,

    pub color: Color,
}

impl Component for Alert {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let class = ctx
            .props()
            .color
            .alert()
            .with_class(ctx.props().class.clone());

        html!(
            <div
                class={class.to_string()}
                style={ctx.props().style.to_string()}
                role="alert"
            >
                {ctx.props().children.clone()}
            </div>
        )
    }
}
