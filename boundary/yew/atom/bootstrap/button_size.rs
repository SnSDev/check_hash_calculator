#[allow(unused)]
#[derive(PartialEq, Eq, Clone, Copy)]
pub enum ButtonSize {
    Large,
    Small,
}

impl std::fmt::Display for ButtonSize {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Large => write!(f, "btn-lg"),
            Self::Small => write!(f, "btn-sm"),
        }
    }
}
