use crate::atom::html::Class;

#[allow(unused)]
#[derive(PartialEq, Eq, Clone, Copy)]
pub enum SpinnerSize {
    Small,
}

impl From<SpinnerSize> for Class {
    fn from(value: SpinnerSize) -> Self {
        match value {
            SpinnerSize::Small => Class::new("spinner-border-sm"),
        }
    }
}
