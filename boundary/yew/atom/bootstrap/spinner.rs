use crate::atom::{
    bootstrap::{Color, SpinnerSize},
    html::{Class, Span, Style},
};

use yew::prelude::*;

#[derive(Default)]
pub struct Spinner {}

#[derive(PartialEq, Properties)]
pub struct Properties {
    pub caption: String,

    pub color: Option<Color>,
    pub size: Option<SpinnerSize>,

    #[prop_or_default]
    pub style: Style,

    #[prop_or_default]
    pub class: Class,
}

impl Component for Spinner {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let mut class = Class::new("spinner-border");

        if let Some(size) = ctx.props().size {
            class = class.with_class(size.into());
        }

        class = class
            .with_class(ctx.props().color.unwrap_or(Color::Primary).text())
            .with_class(ctx.props().class.clone());

        html!(
            <div
                class={class.to_string()}
                style={ctx.props().style.to_string()}
                role="status"
            >
                <Span class={Class::new("visually-hidden")}>
                    {ctx.props().caption.clone()}
                </Span>
            </div>
        )
    }
}
