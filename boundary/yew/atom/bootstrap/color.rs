use crate::atom::html::Class;

#[allow(unused)]
#[derive(PartialEq, Eq, Clone, Copy)]
pub enum Color {
    Primary,
    Secondary,
    Success,
    Danger,
    Warning,
    Info,
    Light,
    Dark,
}

impl std::fmt::Display for Color {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Color::Primary => write!(f, "primary"),
            Color::Secondary => write!(f, "secondary"),
            Color::Success => write!(f, "success"),
            Color::Danger => write!(f, "danger"),
            Color::Warning => write!(f, "warning"),
            Color::Info => write!(f, "info"),
            Color::Light => write!(f, "light"),
            Color::Dark => write!(f, "dark"),
        }
    }
}

impl Color {
    pub fn button(self) -> Class {
        Class::new("btn").with(&format!("btn-{self}"))
    }

    pub fn button_outline(self) -> Class {
        Class::new("btn").with(&format!("btn-outline-{self}"))
    }

    pub fn text(self) -> Class {
        Class::new(&format!("text-{self}"))
    }

    pub fn alert(self) -> Class {
        Class::new("alert").with(&format!("alert-{self}"))
    }
}
