use crate::atom::{
    bootstrap::{ButtonFill, ButtonSize, Color},
    html::Class,
};
use yew::prelude::*;

#[derive(Default)]
pub struct Button {}

#[derive(PartialEq, Properties)]
pub struct Properties {
    #[prop_or_default]
    pub children: Children,
    pub size: Option<ButtonSize>,
    pub color: Option<Color>,
    pub fill: Option<ButtonFill>,
    pub on_click: Option<Callback<MouseEvent>>,
}

impl Component for Button {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let color = ctx.props().color.unwrap_or(Color::Primary);
        let color_css = match ctx.props().fill.unwrap_or_default() {
            ButtonFill::Fill => color.button(),
            ButtonFill::Outline => color.button_outline(),
        };

        let mut class = Class::default().with("button").with_class(color_css);

        let size = match ctx.props().size {
            Some(s) => format!("{s}"),
            None => String::new(),
        };

        if ctx.props().size.is_some() {
            class = class.with(&size);
        }

        html!(
            <button
                type="button"
                class={class.to_string()}
                onclick={ctx.props().on_click.clone().unwrap_or_else(|| Callback::from(|_e| ()))}
            >
                {ctx.props().children.clone()}
            </button>
        )
    }
}
