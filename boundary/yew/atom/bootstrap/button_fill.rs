#[derive(Default, PartialEq, Eq, Clone, Copy)]
#[allow(unused)]
pub enum ButtonFill {
    Outline,

    #[default]
    Fill,
}
