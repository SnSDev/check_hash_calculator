use crate::atom::{
    bootstrap::{Alert, Color},
    bootstrap_icons::Bi,
    html::Style,
};
use yew::prelude::*;

#[derive(Default)]
pub struct CacheStateError {}

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum CacheErrorState {
    UnableToDetermine,
    UnableToFill,
}

impl std::fmt::Display for CacheErrorState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::UnableToDetermine => "Unable to determine cache status",
                Self::UnableToFill => "Unable to Cache App",
            }
        )
    }
}

#[derive(PartialEq, Properties)]
pub struct Properties {
    pub error: CacheErrorState,
}

impl Component for CacheStateError {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let style = Style::default()
            .with("margin", "0")
            .with("padding", "0.1rem 0.25rem")
            .with("display", "inline-block");

        html!(
            <Alert color={Color::Danger} style={style}>
                { Bi::DatabaseExclamation.into() }
                { " " }{ ctx.props().error }
            </Alert>
        )
    }
}
