use yew::prelude::*;

#[allow(unused)]
pub enum Bi {
    App,
    ArrowClockwise,
    ArrowCounterClockwise,
    ArrowRepeat,
    CalendarEvent,
    Cart,
    CartPlus,
    Check,
    Circle1,
    Circle2,
    Circle3,
    Circle4,
    Circle5,
    CircleHalf,
    Clipboard,
    CloudCheck,
    CloudFill,
    CloudSlash,
    ConeStriped,
    Database,
    DatabaseCheck,
    DatabaseExclamation,
    DatabaseSlash,
    ExclamationTriangle,
    Eye,
    EyeDropper,
    EyeFill,
    EyeSlash,
    EyeSlashFill,
    FileEarmarkText,
    Fire,
    GeoAlt,
    Globe,
    Hurricane,
    Moon,
    People,
    PersonCircle,
    PinMap,
    PlayCircle,
    QuestionOctagon,
    Save,
    Sun,
    Tag,
    Tornado,
    X,
    XLg,
}

impl std::fmt::Display for Bi {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        #[allow(
            clippy::enum_glob_use
            // , reason = "Excessive usage of Bi:: otherwise" // TODO: Enable when issue #54503 resolved
        )]
        use Bi::*;
        write!(
            f,
            "{}",
            match self {
                App => "app",
                ArrowClockwise => "arrow-clockwise",
                ArrowCounterClockwise => "arrow-counterclockwise",
                ArrowRepeat => "arrow-repeat",
                CalendarEvent => "calendar-event", // When
                Cart => "cart",
                CartPlus => "cart-plus",
                Check => "check",
                Circle1 => "1-circle",
                Circle2 => "2-circle",
                Circle3 => "3-circle",
                Circle4 => "4-circle",
                Circle5 => "5-circle",
                CircleHalf => "circle-half",
                Clipboard => "clipboard",
                CloudCheck => "cloud-check",
                CloudFill => "cloud-fill",
                CloudSlash => "cloud-slash",
                ConeStriped => "cone-striped", // Disaster Other
                Database => "database",
                DatabaseCheck => "database-check",
                DatabaseExclamation => "database-exclamation",
                DatabaseSlash => "database-slash",
                ExclamationTriangle => "exclamation-triangle",
                Eye => "eye",
                EyeDropper => "eyedropper",
                EyeFill => "eye-fill",
                EyeSlash => "eye-slash",
                EyeSlashFill => "eye-slash-fill",
                FileEarmarkText => "file-earmark-text",
                Fire => "fire", // Disaster Response
                GeoAlt => "geo-alt",
                Globe => "globe",
                Hurricane => "hurricane", // Disaster Response
                Moon => "moon",
                People => "bi-people",           // Who2
                PersonCircle => "person-circle", // Who
                PinMap => "pin-map",             // Where
                PlayCircle => "play-circle",
                QuestionOctagon => "question-octagon",
                Save => "save",
                Sun => "sun",
                Tag => "tag",         // What
                Tornado => "tornado", // Disaster Response
                X => "x",
                XLg => "x-lg",
            }
        )
    }
}

impl Bi {
    // Can't impl From<Bi> for VNode or Html
    pub fn into(self) -> yew::virtual_dom::VNode {
        let class = format!("bi bi-{self}");
        html!(
            <i class={class}></i>
        )
    }
}
