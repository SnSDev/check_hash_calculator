use crate::atom::html::{Class, Style};
use std::string::ToString;
use yew::prelude::*;

#[derive(Default)]
pub struct Button {}

#[derive(PartialEq, Properties)]
pub struct Properties {
    #[prop_or_default]
    pub children: Children,

    pub class: Option<Class>,
    pub style: Option<Style>,
    pub on_click: Option<Callback<MouseEvent>>,
}

impl Component for Button {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let on_click = ctx
            .props()
            .on_click
            .clone()
            .unwrap_or_else(|| Callback::from(|_| ()));

        html!(
            <button
                type="button"
                class={ctx.props().class.as_ref().map(ToString::to_string).unwrap_or_default()}
                style={ctx.props().style.as_ref().map(ToString::to_string).unwrap_or_default()}
                onclick={on_click}
            >
                {ctx.props().children.clone()}
            </button>
        )
    }
}
