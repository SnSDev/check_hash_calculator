#[derive(Default, Clone, PartialEq)]
pub struct Style(Vec<Stylet>);

#[derive(PartialEq, Eq, Clone)]
struct Stylet {
    pub key: String,
    pub value: String,
}

impl std::fmt::Display for Stylet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:{};", self.key, self.value)
    }
}

impl std::fmt::Display for Style {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for s in &self.0 {
            write!(f, "{s}")?;
        }
        Ok(())
    }
}

impl Style {
    pub fn new(key: &str, value: &str) -> Self {
        Self(vec![Self::new_stylet(key, value)])
    }

    pub fn with(self, key: &str, value: &str) -> Self {
        self.with_stylet(Self::new_stylet(key, value))
    }

    pub fn with_style(self, style: Style) -> Self {
        let mut r = self;
        for stylet in style.0 {
            r = r.with_stylet(stylet);
        }
        r
    }

    pub fn with_style_option(self, style: Option<Style>) -> Self {
        match style {
            Some(style) => self.with_style(style),
            None => self,
        }
    }

    fn with_stylet(mut self, mut stylet: Stylet) -> Self {
        for v in &mut self.0 {
            if v.key == stylet.key {
                std::mem::swap(v, &mut stylet);
                return self;
            }
        }
        self.0.push(stylet);
        self
    }

    fn new_stylet(key: &str, value: &str) -> Stylet {
        Stylet {
            key: key.to_owned(),
            value: value.to_owned(),
        }
    }
}
