use crate::atom::html::Style;

#[derive(PartialEq, Eq, Clone, Copy)]
#[allow(unused)]
pub enum TextDecoration {
    None,
}

impl From<TextDecoration> for Style {
    fn from(value: TextDecoration) -> Self {
        Style::new(
            "text-decoration",
            match value {
                TextDecoration::None => "none",
            },
        )
    }
}
