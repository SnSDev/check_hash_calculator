use crate::atom::html::{Class, Style};
use yew::prelude::*;

#[derive(Default)]
pub struct Span {}

#[derive(PartialEq, Properties)]
pub struct Properties {
    #[prop_or_default]
    pub children: Children,

    #[prop_or_default]
    pub title: String,

    #[prop_or_default]
    pub class: Class,

    #[prop_or_default]
    pub style: Style,
}

impl Component for Span {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html!(
         <span
            title={ctx.props().title.clone()}
            class={ctx.props().class.to_string()}
            style={ctx.props().style.to_string()}
        >
            {ctx.props().children.clone()}
        </span>
        )
    }
}
