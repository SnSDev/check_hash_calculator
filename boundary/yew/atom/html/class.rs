// pub trait Class {
//     fn render(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result;
// }

// // impl<'a> Class for &'a str {
// //     fn render(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
// //         write!(f, "{self}")
// //     }
// // }

// // impl Class for String {
// //     fn render(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
// //         write!(f, "{self}")
// //     }
// // }

// impl<D: std::fmt::Display> Class for D {
//     fn render(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         write!(f, "{self}")
//     }
// }

// #[derive(Default)]
// pub struct ClassVec<'a> {
//     inner: Vec<&'a dyn Class>,
// }

// impl<'a> Class for ClassVec<'a> {
//     fn render(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         let mut first = true;
//         for v in &self.inner {
//             if first {
//                 write!(f, " ")?;
//             }
//             first = false;
//             v.render(f)?;
//         }

//         Ok(())
//     }
// }

// impl<'a> ClassVec<'a> {
//     pub fn with_class(mut self, v: &'a dyn Class) -> Self {
//         self.inner.push(v);
//         self
//     }

//     pub fn to_string(&self) -> String {
//         format!("{}", Fmt(|f| self.render(f)))
//     }
// }

use std::fmt::Display;

#[derive(Default, PartialEq, Clone)]
pub struct Class(Vec<String>); // TODO: Replace with dyn Class

impl Class {
    pub fn new(class: &str) -> Self {
        Self(vec![class.to_owned()])
    }

    pub fn with(mut self, class: &str) -> Self {
        self.0.push(class.to_owned());
        self
    }

    pub fn with_class(mut self, class: Class) -> Self {
        self.0.extend(class.0);
        self
    }
}

impl Display for Class {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.iter().format_implode(f, " ")
    }
}

trait FormatImplode<D: Display>: Iterator<Item = D> + Sized {
    fn format_implode<G: Display>(
        mut self,
        f: &mut std::fmt::Formatter<'_>,
        glue: G,
    ) -> std::fmt::Result {
        if let Some(v) = self.next() {
            write!(f, "{v}")?;
        }
        for v in self {
            write!(f, "{glue}{v}")?;
        }
        Ok(())
    }
}

impl<'a> FormatImplode<&'a String> for std::slice::Iter<'a, String> {}
