pub enum InputType {
    Password,
    Text,
}

impl std::fmt::Display for InputType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                InputType::Text => "text",
                InputType::Password => "password",
            },
        )
    }
}
