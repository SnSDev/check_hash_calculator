use crate::atom::{bootstrap::Color, bootstrap_icons::Bi};
use crate::emeraldinspirations_theme::atom::FieldValidity;
use yew::prelude::*;

#[derive(Default)]
pub struct GeneratePepperButtonGroupItem {}

#[derive(PartialEq, yew::Properties)]
pub struct Properties {
    pub on_click: Callback<()>,
    pub valid: FieldValidity,
}

pub enum Message {
    GeneratePepper,
}

impl Component for GeneratePepperButtonGroupItem {
    type Message = Message;
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn update(&mut self, ctx: &Context<Self>, _msg: Self::Message) -> bool {
        ctx.props().on_click.emit(());
        false
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();

        let button_class = match ctx.props().valid {
            FieldValidity::Invalid => Color::Primary.button(),
            v => Color::from(v).button_outline(),
        };

        html!(
            <button
                class={button_class.to_string()}
                type="button"
                title="Generate Random Pepper"
                onclick={link.callback(|_| Message::GeneratePepper)}
            >{ Bi::ArrowClockwise.into() }</button>
        )
    }
}
