use crate::atom::{
    bootstrap::{Link, Spinner, SpinnerSize},
    bootstrap_icons::Bi,
    cache_state_error::CacheErrorState,
    html::{Span, Style, TextDecoration},
    yew::MAINTAIN_COMPONENT,
    CacheStateError,
};
use yew::prelude::*;

#[derive(Default)]
pub struct CacheStateLink {}

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum Message {
    CacheApp,
    DeleteCache,
}

#[derive(PartialEq, Eq, Clone, Copy, Default)]
#[allow(unused)]
pub enum CacheState {
    #[default]
    Unknown,
    UnableToDetermine,
    Present,
    Empty,
    Verifying,
    UnableToFill,
}

#[derive(PartialEq, Properties)]
pub struct Properties {
    pub state: CacheState,
    pub callback: Callback<Message>,
}

impl Component for CacheStateLink {
    type Message = Message;
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        ctx.props().callback.emit(msg);
        MAINTAIN_COMPONENT
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();

        let cache_status: Html = match ctx.props().state {
            CacheState::Empty => html!(
                <Span title={"Cache Empty"}>
                    {Bi::Database.into()}
                    { " " }
                    <Link
                        on_click={link.callback(|_| Message::CacheApp)}
                        text_decoration={TextDecoration::None}
                    >
                        { "Cache App" }
                    </Link>
                </Span>
            ),
            CacheState::Present => html!(
                <Span title="Cache Present">
                    {Bi::DatabaseCheck.into()}
                    { " " }
                    <Link
                        on_click={link.callback(|_| Message::DeleteCache)}
                        text_decoration={TextDecoration::None}
                    >
                        { "Delete Cache" }
                    </Link>
                </Span>
            ),
            CacheState::UnableToDetermine => html!(
                <CacheStateError error={CacheErrorState::UnableToDetermine } />
            ),
            CacheState::UnableToFill => html!(
                <CacheStateError error={CacheErrorState::UnableToFill} />
            ),
            CacheState::Unknown => html!(
                <Span
                    style={Style::new("opacity", "0.5")}
                >
                    {Bi::Database.into()}
                    { " Cache Status Unknown" }
                </Span>
            ),
            CacheState::Verifying => {
                let spinner_style = Style::default()
                    .with("position", "relative")
                    .with("left", "-1rem")
                    .with("display", "inline-block")
                    .with("opacity", "0.75");

                html!(
                    <Span title="Verifying Cache Status">
                        {Bi::Database.into()}
                        <div style="display: inline-block;width: 0;">
                            <Spinner
                                caption={"Verifying Cache Status"}
                                size={SpinnerSize::Small}
                                style={spinner_style}
                            />
                        </div>
                        { " Verifying Cache Status"}
                    </Span>
                )
            }
        };
        html!({ cache_status })
    }
}
