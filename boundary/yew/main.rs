#![warn(clippy::pedantic)]
#![warn(clippy::str_to_string)]
#![warn(clippy::string_to_string)]
#![allow(clippy::let_unit_value)] // TODO: Find out why components in html!() trigger this

mod atom;
mod emeraldinspirations_theme;
mod molecule;
mod organism;
mod page;
mod template;

use yew::prelude::*;

#[derive(Default)]
struct CounterComponent {}

impl Component for CounterComponent {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        use crate::emeraldinspirations_theme::organism::Navbar;
        use crate::organism::CheckHashCalculatorForm;
        html! {
            <>
                <Navbar />
                <CheckHashCalculatorForm />
            </>
        }
    }
}

fn main() {
    yew::start_app::<CounterComponent>();
}
