use crate::atom::yew::RE_RENDER_COMPONENT;
use crate::molecule::{
    account_number_field::FieldValidity as AccountNumberValidity,
    account_number_field::Message as AccountNumberMessage,
    example_check_bar::Message as ExampleCheckMessage,
    pepper_field::FieldValidity as PepperValidity,
    pepper_field::Message as PepperMessage,
    routing_number_field::FieldValidity as RoutingNumberValidity,
    routing_number_field::Message as RoutingNumberMessage,
    routing_number_field::EXAMPLE, AccountNumberField, AppCardHeader,
    CheckHashField, ExampleCheckBar, PepperField, RoutingNumberField,
};
use check_hash_calculator::calculate_check_hash_use_case::{
    calculate_check_hash, RequestModel,
};
use check_hash_calculator::data_object::CheckHash;
use check_hash_calculator::data_object::{
    array_of_base::Error as ArrayOfBaseError, AccountNumber, Pepper,
    RoutingNumber,
};
use check_hash_calculator::entity::{DecimalParser, HexadecimalParser};
use check_hash_calculator::generate_pepper_use_case::{
    generate_pepper, RequestModel as GeneratePepperRequestModel,
};
use check_hash_calculator::presenter::array_of_base_16::Presenter as ArrayOfHexPresenter;
use check_hash_calculator::wrapper::RandomHexRand;
use yew::prelude::*;

#[derive(Default)]
pub struct CheckHashCalculatorForm {
    pepper: String,
    pepper_validity: PepperValidity,
    routing_number: String,
    routing_number_validity: RoutingNumberValidity,
    account_number: String,
    account_number_validity: AccountNumberValidity,
    check_hash: String,
    check_hash_presenter: ArrayOfHexPresenter<7>,
}

pub enum Message {
    UpdatePepper(String),
    GeneratePepper,
    UpdateRoutingNumber(String),
    UpdateAccountNumber(String),
    ApplyExampleCheck(ExampleCheckMessage),
}

impl From<PepperMessage> for Message {
    fn from(value: PepperMessage) -> Self {
        match value {
            PepperMessage::GeneratePepper => Self::GeneratePepper,
            PepperMessage::UpdatePepper(v) => Self::UpdatePepper(v),
        }
    }
}

impl From<RoutingNumberMessage> for Message {
    fn from(value: RoutingNumberMessage) -> Self {
        match value {
            RoutingNumberMessage::Update(v) => Self::UpdateRoutingNumber(v),
        }
    }
}

impl From<AccountNumberMessage> for Message {
    fn from(value: AccountNumberMessage) -> Self {
        match value {
            AccountNumberMessage::Update(v) => Self::UpdateAccountNumber(v),
        }
    }
}

impl From<ExampleCheckMessage> for Message {
    fn from(value: ExampleCheckMessage) -> Self {
        Self::ApplyExampleCheck(value)
    }
}

impl Component for CheckHashCalculatorForm {
    type Message = Message;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        self.process_message(msg);

        let (Some(pepper), Some(routing_number), Some(account_number)) = (
            self.validate_pepper(),
            self.validate_routing_number(),
            self.validate_account_number(),
        ) else {
            self.check_hash.clear();
            return RE_RENDER_COMPONENT;
        };

        let check_hash_u8_array: CheckHash = calculate_check_hash(
            RequestModel::new(routing_number, account_number, pepper),
        )
        .expect("1668120916 - No error variants")
        .check_hash;

        self.check_hash = self
            .check_hash_presenter
            .render(&check_hash_u8_array)
            .to_string();

        RE_RENDER_COMPONENT
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();

        let pepper_callback: Callback<PepperMessage> =
            link.callback(Message::from);
        let routing_number_callback: Callback<RoutingNumberMessage> =
            link.callback(Message::from);
        let account_number_callback: Callback<AccountNumberMessage> =
            link.callback(Message::from);
        let example_check_callback: Callback<ExampleCheckMessage> =
            link.callback(Message::from);

        html!(
            <div class="card">
            <AppCardHeader />
            <div class="card-body">
                <PepperField
                    callback={pepper_callback}
                    valid={self.pepper_validity}
                    value={self.pepper.clone()}
                />
                <RoutingNumberField
                    callback={routing_number_callback}
                    valid={self.routing_number_validity}
                    value={self.routing_number.clone()}
                />
                <AccountNumberField
                    callback={account_number_callback}
                    valid={self.account_number_validity}
                    value={self.account_number.clone()}
                />
                <ExampleCheckBar callback={example_check_callback} />
            </div>
            <div class="card-footer">
                <CheckHashField
                    value={self.check_hash.clone()}
                />
            </div>
        </div>
        )
    }
}

impl CheckHashCalculatorForm {
    fn validate_pepper(&mut self) -> Option<Pepper> {
        match HexadecimalParser::parse(&self.pepper) {
            Ok(p) => {
                self.pepper_validity = PepperValidity::Valid;
                Some(p)
            }
            Err(ArrayOfBaseError::Empty) => {
                self.pepper_validity = PepperValidity::Empty;
                None
            }
            Err(ArrayOfBaseError::Undersized) => {
                self.pepper_validity = PepperValidity::TooShort {
                    size: self.pepper.len(),
                };
                None
            }
            Err(ArrayOfBaseError::Oversized) => {
                self.pepper_validity = PepperValidity::TooLong {
                    size: self.pepper.len(),
                };
                None
            }
            Err(ArrayOfBaseError::InvalidValue) => {
                self.pepper_validity = PepperValidity::Invalid;
                None
            }
        }
    }

    fn validate_routing_number(&mut self) -> Option<RoutingNumber> {
        match DecimalParser::parse(&self.routing_number) {
            Ok(p) => {
                self.routing_number_validity = RoutingNumberValidity::Valid;
                Some(p)
            }
            Err(ArrayOfBaseError::Empty) => {
                self.routing_number_validity = RoutingNumberValidity::Empty;
                None
            }
            Err(ArrayOfBaseError::Undersized) => {
                self.routing_number_validity =
                    RoutingNumberValidity::TooShort {
                        size: self.routing_number.len(),
                    };
                None
            }
            Err(ArrayOfBaseError::Oversized) => {
                self.routing_number_validity = RoutingNumberValidity::TooLong {
                    size: self.routing_number.len(),
                };
                None
            }
            Err(ArrayOfBaseError::InvalidValue) => {
                self.routing_number_validity = RoutingNumberValidity::Invalid;
                None
            }
        }
    }

    fn validate_account_number(&mut self) -> Option<AccountNumber> {
        match DecimalParser::parse_pad(&self.account_number) {
            Ok(p) => {
                self.account_number_validity = AccountNumberValidity::Valid {
                    size: self.account_number.len(),
                };
                Some(p)
            }
            Err(ArrayOfBaseError::Empty) => {
                self.account_number_validity = AccountNumberValidity::Empty;
                None
            }
            Err(ArrayOfBaseError::Undersized) => {
                unreachable!("1668120137")
            }
            Err(ArrayOfBaseError::Oversized) => {
                self.account_number_validity = AccountNumberValidity::TooLong {
                    size: self.account_number.len(),
                };
                None
            }
            Err(ArrayOfBaseError::InvalidValue) => {
                self.account_number_validity = AccountNumberValidity::Invalid;
                None
            }
        }
    }

    fn process_message(&mut self, msg: Message) {
        match msg {
            Message::GeneratePepper => {
                let presenter = ArrayOfHexPresenter::default();

                let new_pepper = generate_pepper(GeneratePepperRequestModel {
                    randomizer: RandomHexRand::default(),
                })
                .ok()
                .expect("1668047238 - Unreachable (No error variants)")
                .pepper;

                self.pepper = presenter.render(&new_pepper).to_string();
            }
            Message::UpdatePepper(mut v) => {
                v.make_ascii_lowercase();
                self.pepper = v;
            }
            Message::UpdateRoutingNumber(v) => {
                self.routing_number = v;
            }
            Message::UpdateAccountNumber(v) => {
                self.account_number = v;
            }
            Message::ApplyExampleCheck(c) => {
                self.routing_number = EXAMPLE.to_owned();
                self.account_number = match c {
                    ExampleCheckMessage::ExampleCheck1 => "123456789012",
                    ExampleCheckMessage::ExampleCheck2 => "000000000001",
                    ExampleCheckMessage::ExampleCheck3 => "222222222222",
                    ExampleCheckMessage::ExampleCheck4 => "943416785318",
                    ExampleCheckMessage::ExampleCheck5 => "749749320948",
                }
                .to_owned();
            }
        }
    }
}
