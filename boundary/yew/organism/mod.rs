//! A grouping of [`molecule`][crate::molecule]  with record-level relation
//!
//! Example:
//!
//! - A form with all it's fields and "Submit" / "Reset" buttons
//! - A card with title, image, text, and relative actions
//! - A toolbar with menus, search field, and logo

mod check_hash_calculator_form;

pub use check_hash_calculator_form::CheckHashCalculatorForm;
