use crate::emeraldinspirations_theme::atom::{
    NavbarBranding, NavbarDivider, NavbarHamburger, NavbarItem,
};
use yew::prelude::*;

#[derive(Default)]
pub struct Navbar {}

impl Component for Navbar {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html!(
        <header
            class="navbar sticky-top navbar-expand-lg navbar-dark"
            style="background-color: rgb(2, 90, 25); padding: 0rem 0.5rem"
        >
            <div
                class="container-fluid"
                style="padding-left: 0px"
            >
                <NavbarBranding />
                <NavbarHamburger />
                <div class="collapse navbar-collapse" >
                    <span
                        class="navbar-text"
                        style="width: 100%"
                    ></span>
                    <div
                        class="collapse navbar-collapse"
                        id="navbarNavDropdown"
                    >
                        <ul class="navbar-nav" >
                            <NavbarItem display={ "Software" } href={ "#" } />
                            <NavbarItem display={ "Design" } href={ "#" } />
                            <NavbarItem display={ "Juggling" } href={ "#" } />
                            <NavbarDivider />
                            <NavbarDivider />
                            <NavbarItem display={ "Public" } href={ "#" } disabled={ true } />
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        )
    }
}
