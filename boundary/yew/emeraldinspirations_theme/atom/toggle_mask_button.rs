use crate::atom::{bootstrap::Color, bootstrap_icons::Bi};
use crate::emeraldinspirations_theme::atom::{FieldValidity, PrivateFieldMask};
use yew::prelude::*;

#[derive(Default)]
pub struct ToggleMaskButton {}

#[derive(PartialEq, yew::Properties)]
pub struct Properties {
    pub mask: PrivateFieldMask,
    pub on_click: Callback<()>,
    pub valid: FieldValidity,
}

pub enum Message {
    ToggleMask,
}

impl Component for ToggleMaskButton {
    type Message = Message;
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn update(&mut self, ctx: &Context<Self>, _msg: Self::Message) -> bool {
        ctx.props().on_click.emit(());
        false
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();

        html!(
            <button
                class={Color::from(ctx.props().valid).button_outline().to_string()}
                type="button"
                tabindex={"-1"}
                onclick={link.callback(|_| Message::ToggleMask)}
                title="Mask Field (ctrl + shift + L)"
            >
                {Bi::from(ctx.props().mask).into()}
            </button>
        )
    }
}
