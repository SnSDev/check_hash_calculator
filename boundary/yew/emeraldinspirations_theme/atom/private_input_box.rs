use crate::atom::html::{Class, InputType};
use crate::emeraldinspirations_theme::atom::{FieldValidity, PrivateFieldMask};
use wasm_bindgen::JsCast;
use web_sys::HtmlInputElement;
use yew::prelude::*;

pub struct PrivateInputBox {}

#[derive(PartialEq, yew::Properties)]
pub struct Properties {
    pub mask: PrivateFieldMask,
    pub name: String,
    pub on_change: Callback<String>, // Can't derive Eq
    pub valid: FieldValidity,
    pub value: String,
}

impl Component for PrivateInputBox {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        let on_change = ctx.props().on_change.clone();
        let on_input = Callback::from(move |event: InputEvent| {
            let target = event.target().expect("1668026987 - Unreachable");
            let input = target.unchecked_into::<HtmlInputElement>();
            let value = input.value();
            on_change.emit(value);
        });

        let input_class = {
            let class = Class::new("form-control");
            match ctx.props().valid {
                FieldValidity::Valid => class.with("is-valid"), // TODO: Validity Class enum
                FieldValidity::Invalid => class.with("is-invalid"),
                FieldValidity::Empty => class,
            }
        };

        html!(
            <input
                type={InputType::from(ctx.props().mask).to_string()}
                class={input_class.to_string()}
                value={ctx.props().value.clone()}
                id={ctx.props().name.clone()}
                oninput={on_input}
            />
        )
    }
}
