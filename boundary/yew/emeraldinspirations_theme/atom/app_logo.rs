use yew::prelude::*;

#[derive(Default)]
pub struct AppLogo {}

impl Component for AppLogo {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html!(
        <img
            src="icon-256x256.png"
            alt="App Icon"
            style="
                    height: 1em;
                    margin-right: 0.25em;
                    position: relative;
                    top: -0.1em;
            " />
            )
    }
}
