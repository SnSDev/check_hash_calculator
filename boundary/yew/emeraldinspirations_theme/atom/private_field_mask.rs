use crate::atom::{bootstrap_icons::Bi, html::InputType};

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum PrivateFieldMask {
    Mask,
    Show,
}

impl From<PrivateFieldMask> for Bi {
    fn from(mask: PrivateFieldMask) -> Self {
        match mask {
            PrivateFieldMask::Mask => Self::EyeSlash,
            PrivateFieldMask::Show => Self::EyeFill,
        }
    }
}

impl From<PrivateFieldMask> for InputType {
    fn from(mask: PrivateFieldMask) -> Self {
        match mask {
            PrivateFieldMask::Mask => Self::Password,
            PrivateFieldMask::Show => Self::Text,
        }
    }
}

impl PrivateFieldMask {
    pub fn toggle(&mut self) {
        *self = match self {
            Self::Mask => Self::Show,
            Self::Show => Self::Mask,
        }
    }
}

impl Default for PrivateFieldMask {
    fn default() -> Self {
        Self::Mask
    }
}
