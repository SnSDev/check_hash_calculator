use yew::prelude::*;

#[derive(Default)]
pub struct FieldLabel {}

#[derive(Properties, PartialEq)]
pub struct Properties {
    pub field_name: String,
    pub display: String,
}

impl Component for FieldLabel {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html!(
            <label for={ ctx.props().field_name.clone() } class="form-label">
                { ctx.props().display.clone() }
            </label>
        )
    }
}
