use yew::prelude::*;

#[derive(Default)]
pub struct NavbarDivider {}

impl Component for NavbarDivider {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html!(
            <li
                class="nav-item"
                style="
                    border-left: 1px solid rgba(250, 250, 250, 0.25);
                    border-right: 1px solid rgba(0, 0, 0, 0.25);
                    margin-left: 0.25em;
                    margin-right: 0.25em;
                "
            ></li>
        )
    }
}
