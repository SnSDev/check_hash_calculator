use yew::prelude::*;

#[derive(Default)]
pub struct NavbarBranding {}

#[derive(PartialEq, Properties)]
pub struct Properties {
    pub app_name: Option<String>,
}

impl Component for NavbarBranding {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html!(
            <a class="navbar-brand" href="http://emeraldinspirations.xyz">
                <img
                    src="LogoWhite.svg"
                    alt="EI Logo"
                    style="
                        height: 1em;
                        top: -0.1em;
                        position: relative;
                        margin-right: 0.25em;
                    "
                />
                { "emeraldinspirations.xyz" }
            </a>
        )
    }
}
