use crate::emeraldinspirations_theme::atom::FieldValidity;
use yew::prelude::*;

#[derive(Default)]
pub struct FieldHelp {}

#[derive(Properties, PartialEq)]
pub struct Properties {
    pub valid: FieldValidity,
    pub display: String,
}

impl Component for FieldHelp {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html!(
            <div class={help_class(ctx.props().valid)} style="display:block;">
                { ctx.props().display.clone() }
            </div>
        )
    }
}

fn help_class<'a>(valid: FieldValidity) -> &'a str {
    match valid {
        FieldValidity::Empty => "form-text",
        FieldValidity::Valid => "valid-feedback",
        FieldValidity::Invalid => "invalid-feedback",
    }
}
