use crate::atom::bootstrap_icons::Bi;
use yew::prelude::*;

#[derive(Default)]
pub struct WebAppStatus {
    state: StatusState,
}

#[derive(Default, PartialEq, Eq, Clone, Copy)]
pub enum StatusState {
    PWA,
    #[default]
    Website,
}

impl From<StatusState> for Bi {
    fn from(value: StatusState) -> Self {
        match value {
            StatusState::PWA => Bi::App,
            StatusState::Website => Bi::Globe,
        }
    }
}

#[derive(PartialEq, Properties)]
pub struct Properties {}

impl Component for WebAppStatus {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        let caption = match self.state {
            StatusState::PWA => "Running as PWA",
            StatusState::Website => "Running as Website",
        };

        html!(
            <span title={caption}>
                {Bi::from( self.state).into()}
            </span>
        )
    }
}

/*
window
    .matchMedia('(display-mode: standalone)')
    .addEventListener('change', ({ matches }) => {
        if (matches) {
            $('#installAppButton').hide();
        } else {
            $('#installAppButton').show();
        }
    });
*/
