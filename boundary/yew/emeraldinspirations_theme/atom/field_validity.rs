use crate::atom::bootstrap::Color;

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum FieldValidity {
    Valid,
    Invalid,
    Empty,
}

impl Default for FieldValidity {
    fn default() -> Self {
        Self::Empty
    }
}

impl From<FieldValidity> for Color {
    fn from(v: FieldValidity) -> Color {
        match v {
            FieldValidity::Invalid => Color::Danger,
            FieldValidity::Valid => Color::Success,
            FieldValidity::Empty => Color::Secondary,
        }
    }
}
