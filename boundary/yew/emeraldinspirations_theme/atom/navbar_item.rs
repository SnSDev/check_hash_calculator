use yew::prelude::*;

#[derive(Default)]
pub struct NavbarItem {}

#[derive(PartialEq, Properties)]
pub struct Properties {
    pub href: String,
    pub display: String,
    pub disabled: Option<bool>,
}

impl Component for NavbarItem {
    type Message = ();
    type Properties = Properties;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::default()
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let class = if ctx.props().disabled.unwrap_or_default() {
            "nav-link disabled"
        } else {
            "nav-link"
        };

        html!(
        <li
            class="nav-item"
            style="white-space: nowrap"
        >
            <a
                class={ class.to_owned() }
                aria-current="page"
                href={ ctx.props().href.clone() }
                >{ ctx.props().display.clone() }</a
            >
        </li>
        )
    }
}
