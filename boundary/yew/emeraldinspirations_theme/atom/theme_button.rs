use crate::atom::bootstrap_icons::Bi;

#[derive(Default)]
pub struct ThemeButton {
    pub state: ThemeState,
}

#[allow(unused)]
pub enum ThemeState {
    Day,
    Night,
    Os,
}

impl Default for ThemeState {
    fn default() -> Self {
        Self::Os
    }
}

impl From<ThemeState> for Bi {
    fn from(value: ThemeState) -> Self {
        match value {
            ThemeState::Day => Bi::Sun,
            ThemeState::Night => Bi::Moon,
            ThemeState::Os => Bi::CircleHalf,
        }
    }
}
