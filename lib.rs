#![warn(clippy::pedantic)]
#![warn(clippy::str_to_string)]
#![warn(clippy::string_to_string)]

//! Calculate a SHA256 hash from Check metadata
//!
//! # Problem (Subjective)
//!
//! Client wants to locate donor's respective constituent file from check
//! metadata without storing said metadata in CRM Software.
//!
//! ## When Client
//!
//! - Receives [Donation][donation]
//!
//! ## What Client expects
//!
//! - [Admin Assistant][admin-assistant] informs remote [Bookkeeper][bookkeeper]
//!   of [Donation][donation]
//! - [Bookkeeper][bookkeeper] locates [Constituent File][constituent-file]
//!   using [`Check Metadata`][data_object::CheckMetadata]
//! - [Bookkeeper][bookkeeper] updates [Constituent File][constituent-file] to
//!   include [Donation][donation]
//! - Neither [Bookkeeper][bookkeeper] nor [CRM Software][crm-software] is
//!   aware of [`Check Metadata`][data_object::CheckMetadata]
//!
//! ## What actually happens
//!
//! - [Admin Assistant][admin-assistant] must process donation locally
//! - There is no way to look up [Constituent File][constituent-file] using
//!   [`Check Metadata`][data_object::CheckMetadata]
//! - There is no way to securely inform [Bookkeeper][bookkeeper] of
//!   [`Check Metadata`][data_object::CheckMetadata]
//! - [Bookkeeper][bookkeeper] can not use
//!   [`Check Metadata`][data_object::CheckMetadata] without having said data
//!
//! # Facts (Objective)
//!
//! - [Client][client] is willing to purchase Check Scanner & On-Site
//!   Workstation
//! - [Client][client] has dedicated internet connection, dynamic IP address
//!
//! ## Definitions
//!
//! [admin-assistant]: <definition/index.html#admin-assistant>
//! [bookkeeper]: <definition/index.html#bookkeeper>
//! [check]: <definition/index.html#check>
//! [client]: <definition/index.html#client>
//! [constituent-file]: <definition/index.html#constituent-file>
//! [crm-software]: <definition/index.html#crm-software>
//! [donation]: <definition/index.html#donation>
//! [donor]: <definition/index.html#donor>
//! [financial-institution]: <definition/index.html#financial-institution>
//! [unintended-3rd-party]: <definition/index.html#unintended-3rd-party>
//!
//! - [`AccountNumber`][data_object::AccountNumber] *(Data)*
//! - [Admin Assistant][admin-assistant] *(Actor)*
//! - [Bookkeeper][bookkeeper] *(Actor)*
//! - [Check][check] *(Data)*
//! - [`CheckHash`][data_object::CheckHash] *(Data)*
//! - [`CheckMetadata`][data_object::CheckMetadata] *(Data)*
//! - [Client][client] *(Actor)*
//! - [Constituent File][constituent-file] *(Data)*
//! - [CRM Software][crm-software] *(External Interface)*
//! - [Donation][donation] *(Event)*
//! - [Donor][donor] *(Actor)*
//! - [Financial Institution][financial-institution]
//!   *(External Interface)*
//! - [`Pepper`][data_object::Pepper] *(Data)*
//! - [`RoutingNumber`][data_object::RoutingNumber] *(Data)*
//! - [Unintended 3rd Party][unintended-3rd-party]
//!   *(Actor)*
//!
//! # Assessment
//!
//! - [Bookkeeper][bookkeeper] and [CRM Software][crm-software] only need a
//!   [`CheckHash`][data_object::CheckHash] of the to be able to reliably
//!   locate [Constituent File][constituent-file]
//! - (In 2022) A SHA256 hash is considered "Unbroken" therefore should be
//!   sufficient for [`CheckHash`][data_object::CheckHash]
//! - [`RoutingNumber`][data_object::RoutingNumber] is public information and
//!   specific to [Financial Institution][financial-institution], therefore easy
//!   to guess
//! - [`AccountNumber`][data_object::AccountNumber] is typically sequential,
//!   therefore easy to brute-force if
//!   [`RoutingNumber`][data_object::RoutingNumber] is known
//! - A [`Pepper`][data_object::Pepper] will be needed to ensure the
//!   [`CheckMetadata`][data_object::CheckMetadata] is not able to be
//!   discovered from the [`CheckHash`][data_object::CheckHash]
//! - A 256 bit [`Pepper`][data_object::Pepper] would ensure a brute-force
//!   attack needs to run on all possible [`Pepper`][data_object::Pepper] AND
//!   sequentially all [`AccountNumber`][data_object::AccountNumber] (assuming
//!   [Unintended 3rd Party][unintended-3rd-party] already knew the
//!   [`RoutingNumber`][data_object::RoutingNumber]). This is computationally
//!   prohibitive.
//! - The full 256 bit [`CheckHash`][data_object::CheckHash] is not needed to
//!   provide reasonable assurance of no collisions on relatively small donation
//!   by-check base. Truncating to same 7 hexadecimal number short-hash (with
//!   268,435,456 possibilities) used by [Git](https://git-scm.com/) should
//!   provide similar assurance
//! - Abbreviating [`CheckHash`][data_object::CheckHash] increases the
//!   difficulty of brute-forcing by adding exponentially more false-positives
//! - [`Pepper`][data_object::Pepper] MUST be kept secret from all external
//!   parties (especially [Bookkeeper][bookkeeper] and
//!   [CRM Software][crm-software])
//! - [`Pepper`][data_object::Pepper] MUST NOT change unless compromised,
//!   changing [`Pepper`][data_object::Pepper] will render all previous
//!   [`CheckHash`][data_object::CheckHash]
//!   irrelevant

pub mod calculate_check_hash_use_case;
pub mod data_object;
pub mod definition;
pub mod entity;
pub mod generate_pepper_use_case;
pub mod presenter;
pub mod wrapper;
